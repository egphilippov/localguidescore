import logging
import configparser
import os
import datetime as dt
from pymongo import MongoClient, database

# Удалите существующий файл лога, если он есть, чтобы создавать новый файл во время каждого выполнения
if os.path.isfile("./logging.log"):
    os.remove("./logging.log")

class MyFormatter(logging.Formatter):
    converter=dt.datetime.fromtimestamp
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            s = ct.strftime(datefmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s,%03d" % (t, record.msecs)
        return s

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

console = logging.StreamHandler()
logger.addHandler(console)

# logging.disable(level=logging.INFO)

formatter = MyFormatter(fmt='%(asctime)s %(message)s',datefmt='%Y-%m-%d,%H:%M:%S.%f')
console.setFormatter(formatter)

print('read from os.environ')
MEDIA_MCS_URL=os.environ["MEDIA_MCS_URL"]
MONGODB=os.environ["MONGODB"]

client = MongoClient(MONGODB)
db = client["lg-core"]

methods=["POST"]
media_type="application/json; charset=utf-8"