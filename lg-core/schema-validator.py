from pymongo import MongoClient, database
from bson.raw_bson import RawBSONDocument
from bson.codec_options import CodecOptions
from bson.objectid import ObjectId

codec_options = CodecOptions(document_class=RawBSONDocument)
codec_options = CodecOptions(tz_aware=True)

db.create_collection("user", codec_options=codec_options, **{
   "validator": {
      "$jsonSchema": {
         "bsonType": "object",
         "required": [
         "uuid", "device_id", "platform", "country", "name", "email", "is_host", "photos", "telegram_account", "location", 
         "language", "about", "hourly_rate", "activity", "notification", "is_deleted", "is_disabled", "reviews", "timestamp"
         ],
         "properties": {
            "uuid": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "device_id": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "platform": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "country": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "name": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
             "email": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "is_host": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "photos": {
               "bsonType": "date",
               "description": "must be a string and is required"
            },
            "telegram_account": {
               "bsonType": "date",
               "description": "must be a string and is required"
            },
            "location": {
               "bsonType": "array",
               "description": "array of objectsIds"
            },
            "language": {
               "bsonType": "array",
               "description": "array of objectsIds"
            },
            "about": {
               "bsonType": "array",
               "description": "array of objectsIds"
            }
             "hourly_rate": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "activity": {
               "bsonType": "string",
               "description": "must be a string and is required"
            },
            "notification": {
               "bsonType": "date",
               "description": "must be a string and is required"
            },
            "is_deleted": {
               "bsonType": "date",
               "description": "must be a string and is required"
            },
            "is_disabled": {
               "bsonType": "array",
               "description": "array of objectsIds"
            },
            "reviews": {
               "bsonType": "array",
               "description": "array of objectsIds"
            },
            "timestamp": {
               "bsonType": "array",
               "description": "array of objectsIds"
            }
         }
      }
   }
})

db.create_collection("reviews", codec_options=codec_options, **{
   "validator": {
      "$jsonSchema": {
         "bsonType": "object",
         "required": [ "user_to", "user_from", "text", "timestamp" ],
         "properties": {
            "user_to": {
               "bsonType": "objectId",
               "description": "objectid of user who left review"
            },
            "user_from": {
               "bsonType": "objectId",
               "description": "objectid of user received the review"
            },
            "text": {
               "bsonType": "string",
               "description": "text of review"
            },
            "last_update": {
               "bsonType": "date",
               "description": "must be a string and is required"
            },
         }
      }
   }
})
