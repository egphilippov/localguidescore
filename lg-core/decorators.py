
# from configs import COURIER_MCS_URL
from configs import logger
# from utils import sendTelegramError
from datetime import datetime

def log(f):
    async def wrapper(*args, **kwargs):
        request = args[0]
        try:
            data = await request.json()
        except:
            data = 'no data'
        logger.debug("datetime: {} function_name: {} data: {}".format(datetime.now(), f.__name__, data))
        return await f(*args, **kwargs)
    return wrapper
