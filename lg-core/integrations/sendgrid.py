
# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

API_KEY = "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"

def sendActivationMail(to_email, link):
    message = Mail(
        from_email='hi@localguides.cc',
        to_emails=to_email,
        subject='Welcome to localguides. Please confirm your email',
        html_content='<strong>{}</strong>'.format(link))
    try:
        sg = SendGridAPIClient(API_KEY)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)

def sendResetMail(to_email, link):
    message = Mail(
        from_email='hi@localguides.cc',
        to_emails=to_email,
        subject='Password reset. Please click the link',
        html_content='<strong>{}</strong>'.format(link))
    try:
        sg = SendGridAPIClient(API_KEY)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)
