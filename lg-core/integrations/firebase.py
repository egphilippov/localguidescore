
import re
from oauth2client.service_account import ServiceAccountCredentials
import json
import random
import string
import base64
import hashlib
import binascii
import requests
import pandas as pd
import dateutil.parser
from uuid import uuid4
from pymongo import message
from operator import itemgetter
from bson.objectid import ObjectId
from collections import defaultdict
from starlette.routing import Route
from datetime import datetime, timedelta
from starlette.middleware import Middleware
from starlette.endpoints import HTTPEndpoint
from starlette.applications import Starlette
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import JSONResponse, Response, FileResponse, RedirectResponse
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.authentication import requires, AuthenticationBackend, AuthenticationError, SimpleUser, UnauthenticatedUser, AuthCredentials
from timing_asgi import TimingMiddleware, TimingClient
from timing_asgi.integrations import StarletteScopeToName
from decorators import log
from configs import logger, db, media_type, methods

# https://firebase.google.com/docs/reference/rest/auth#section-verify-custom-token

# # Web API Key
# API_KEY = "AIzaSyCzLAop9Fj3ZVfdKyTAB-vHsVafE1gIAq0" 

# class Auth():

#     async def signup():
#         url = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key={}".format(API_KEY)
#         json = {
#             "email": "1",
#             "password": "1",
#             "returnSecureToken": True
#         }
#         try:
#             r = request.post(url, json=json)
#         except:
#             pass
#         if r.status_code == 200:
#             pass
#             # Response
#             # idToken	string	A Firebase Auth ID token for the newly created user.
#             # email	string	The email for the newly created user.
#             # refreshToken	string	A Firebase Auth refresh token for the newly created user.
#             # expiresIn	string	The number of seconds in which the ID token expires.
#             # localId	string	The uid of the newly created user.
#         else:
#             pass


#     async def signin(request):
#         url = "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key={}".format(API_KEY)
#         json = {
#             "email": "1",
#             "password": "1",
#             "returnSecureToken": True
#         }
#         try:
#             r = request.post(url, json=json)
#         except:
#             pass
#         if r.status_code == 200:
#             pass
#             # Response
#             # idToken	string	A Firebase Auth ID token for the newly created user.
#             # email	string	The email for the newly created user.
#             # refreshToken	string	A Firebase Auth refresh token for the newly created user.
#             # expiresIn	string	The number of seconds in which the ID token expires.
#             # localId	string	The uid of the newly created user.
#         else:
#             pass

        
#     async def oauth(request):
#         try:
#             data = await request.json()
#             user_id = ObjectId(str(data['user_id']))
#         except:
#             return JSONResponse({"message": "add parameters"}, status_code=500)

# class Push():

#     async def send(request):
#         try:
#             data = await request.json()
#             user_id = ObjectId(str(data['user_id']))
#         except:
#             return JSONResponse({"message": "add parameters"}, status_code=500)
#         # get device_id by user_id
#         doc = db.user.find_one({"_id": user_id})
#         url = "https://fcm.googleapis.com/v1/projects/allpapers-development/messages:send"
#         headers = {
#         'Authorization': 'Bearer ' + _get_access_token(),
#         'Content-Type': 'application/json; UTF-8',
#         }
#         body = {"message":{
#                 "token" : doc['device_id'],
#                 "notification": {
#                     "title": "Hey",
#                     "body": "hey"
#                 },
#                 "data": {
#                     "drover": True,
#                     "drover_type": "rate"
#                 }
#                 }}
#         try:
#             r=requests.post(url, headers=headers, json=body)
#             return JSONResponse({"message": "push sent"}, status_code=200, media_type=media_type)
#         except:
#             return JSONResponse({"message": "push sent error"}, status_code=500, media_type=media_type)

# def _get_access_token():
#     """Retrieve a valid access token that can be used to authorize requests.
#     :return: Access token.
#     """
#     SCOPES = ["https://www.googleapis.com/auth/cloud-platform", "https://www.googleapis.com/auth/firebase.messaging"]
#     credentials = ServiceAccountCredentials.from_json_keyfile_name(
#     'allpapers-development-firebase.json', SCOPES)
#     access_token_info = credentials.get_access_token()
#     return access_token_info.access_token
