import json
import random
import string
import base64
import hashlib
import binascii
import requests
import pandas as pd
import dateutil.parser
from uuid import uuid4
from pymongo import message
from operator import itemgetter
from bson.objectid import ObjectId
from collections import defaultdict
from starlette.routing import Route
from datetime import datetime, timedelta
from starlette.middleware import Middleware
from starlette.endpoints import HTTPEndpoint
from starlette.applications import Starlette
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import JSONResponse, Response, FileResponse, RedirectResponse
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.authentication import requires, AuthenticationBackend, AuthenticationError, SimpleUser, UnauthenticatedUser, AuthCredentials
from timing_asgi import TimingMiddleware, TimingClient
from timing_asgi.integrations import StarletteScopeToName
from decorators import log
from configs import logger, db, media_type, methods

from integrations.telegram import sendTelegramError
from integrations.sendgrid import sendActivationMail, sendResetMail

# Only for CRM
class BasicAuthBackend(AuthenticationBackend):
    async def authenticate(self, request):
        if "authorization" not in request.headers:
            return
        auth = request.headers["authorization"]
        # if "cookie" not in request.headers:
        #     return
        # cookie = request.headers["cookie"].replace("cookie=","")
        try:
            scheme, credentials = auth.split()
            if scheme.lower() != 'basic':
                return
            decoded = base64.b64decode(credentials).decode("ascii")
        except (ValueError, UnicodeDecodeError, binascii.Error) as exc:
            raise AuthenticationError('Invalid basic auth credentials')

        login, _, password = decoded.partition(":")
        password_hash = hashlib.sha224(str.encode(password)).hexdigest()

        if db.crm.find({"login":{"$eq": login}}).count() > 0:
            doc = db.crm.find_one({"login":{"$eq": login}})
            if doc['password_hash'] == password_hash:
                # TODO check cookie
                return AuthCredentials(["authenticated"]), SimpleUser(doc["name"])
            else:
                return
        else:
            return

class CustomHeaderMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        response = await call_next(request)
        # print(dir(request))
        with open('performance.txt', 'a') as f:
            f.write("{}\n'".format(request.url.path))
        return response

class PrintTimings(TimingClient):
    def timing(self, metric_name, timing, tags):
        with open('performance.txt', 'a') as f:
            f.write("{} - {} - {}\n'".format(metric_name, timing, tags))

class Mail(HTTPEndpoint):
    
    @staticmethod    
    @log
    async def login(request):
        try:
            data = await request.json()
            data = str(data['data'])
        except:
            return JSONResponse({"message":"app parameters"}, media_type=media_type, status_code=500)
        try:
            decoded = base64.b64decode(data).decode("ascii")
            email, _, password = decoded.partition(":")
        except:
            return JSONResponse({"message":"Wrong password"}, media_type=media_type, status_code=500)

        ### Validation Block Open
        if "@" not in email: #TODO make proper validation
            return JSONResponse({"message":"Provided email is invalid"}, media_type=media_type, status_code=500)
        ### Validation Block Close

        if db.user.find({"$and":[ {"email":email}, {"is_activated":True}]}).count() > 0:
            doc = db.user.find_one({"email":{"$eq": email}})
            if doc['password_hash'] == hashlib.sha224(str.encode(password)).hexdigest():

                cookie = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(20))
                # TODO add cookie to DB

                content = {
                    'token': doc['token'], # TODO добавить шифрование base 64 и чтобы basic auth знал где смотрить логин пароль в crm или в user
                    'user_id': str(doc['_id']),
                    'name': doc['name'],
                    'uuid': doc['uuid'],
                    'email': doc['email'],
                    'notification': {'push': doc['notification']['push'], 'email': doc['notification']['email']},
                    "is_host": doc['is_host'],
                    "photos": doc['photos'],
                    "telegram": doc['telegram'],
                    "location": doc['location'], # enum
                    "language": doc['language'], # enum
                    "activity": doc['activity'], # enum
                    "about": doc['about'],
                    "caption": doc["caption"],
                    "hourly_rate": doc['hourly_rate'], 
                    "is_deleted": doc['is_deleted'],
                    "is_disabled": doc['is_disabled'],
                    "reviews": doc['reviews']
                    }
                return JSONResponse(content, media_type=media_type, status_code=200)
            else:
                return JSONResponse({"message":"Wrong password"}, media_type=media_type, status_code=500)
        return JSONResponse({"message":"User does not exists"}, media_type=media_type, status_code=500)
    
    @staticmethod
    @log
    async def registration(request):
        try:
            data = await request.json()
            name = str(data['name'])
            email = str(data['email'])
            uuid = str(data['uuid'])
            password = str(data['password'])
            device_id = str(data['device_id']) if "device_id" in data.keys() else "0"
            platform = str(data['platform']) if "platform" in data.keys() else "0"
            country = str(request.headers['country']) if "country" in request.headers.keys() else "0"
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        ### Validation Block Open
        if "@" not in email: #TODO make proper validation
            return JSONResponse({"message":"Provided email is invalid"}, media_type=media_type, status_code=500)
        if len(name) == 0:
            return JSONResponse({"message":"Please provide your name"}, media_type=media_type, status_code=500)
        if len(password) < 6:
            return JSONResponse({"message":"Password must be a string with at least six characters"}, media_type=media_type, status_code=500)
        if db.user.find({"$and":[ {"email":email}, {"is_activated":True}]}).count() > 0: 
            return JSONResponse({"message":"An account with this email already exists"}, media_type=media_type, status_code=500)
        ### Validation Block Close

        message = "{}:{}".format(email,password)
        message_bytes = message.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        sendTelegramError(base64_bytes)
        
        dt = datetime.utcnow().replace(microsecond=0)
        token = str(uuid4())
        try:
            _id = db.user.insert({
                    "is_activated": False,
                    "activation_code": token,
                    "password_hash": hashlib.sha224(str.encode(password)).hexdigest(),
                    "device_id": device_id,
                    "uuid": uuid,
                    "token": token,
                    "name": name,
                    "email": email,
                    "gender": "0",
                    "platform": platform,
                    "country": country,
                    "notification": {'push': True, 'email': True},
                    "timestamp": {"create_date": dt, "last_login": dt},
                    "is_host": False,
                    "photos": [],
                    "telegram": "",
                    "location": "", # enum
                    "language": [], # enum
                    "activity": [], # enum
                    "about": "",
                    "caption": "",
                    "hourly_rate": 0, 
                    "is_deleted": False,
                    "is_disabled": False,
                    "reviews": []
            })
        except:
            return JSONResponse({"message":"Database error"}, media_type=media_type, status_code=500)
        ## Send Email Block Open
        try:
            sendActivationMail(email, "https://localguides.cc/v1/mail/activate?token={}".format(token))
            return JSONResponse({"message":"We have sent an email with a confirmation link to your email address"}, status_code=200, media_type=media_type)
        except:
            return JSONResponse({"message":"Provided email is invalid"}, status_code=500, media_type=media_type)
        ## Send Email Block Open
     
    @staticmethod       
    @log
    async def password_reset(request):
        try:
            data = await request.json()
            email = str(data['email'])
        except:
            return JSONResponse({"message":"add parameters"}, status_code=500, media_type=media_type)
        ### Validation Block Open
        if "@" not in email: #TODO make proper validation
            return JSONResponse({"message":"Provided email is invalid"}, media_type=media_type, status_code=500)
        if db.user.find({"email":email}).count() == 0: 
            return JSONResponse({"message":"User does not exists"}, media_type=media_type, status_code=500)
        ### Validation Block Close
        token = str(uuid4())
        try:
            db.user.update_one({"email":email}, {'$set': {'token': token}}, upsert=False)
        except:
            return JSONResponse({"message":"Database error"}, media_type=media_type, status_code=500)
        try:
            sendResetMail(email, "https://localguides.cc/v1/mail/password/set?token={}".format(token))
            return JSONResponse({"message":"We have sent an email with a reset link to your email address"}, status_code=200, media_type=media_type)
        except:
            return JSONResponse({"message":"Provided email is invalid"}, media_type=media_type, status_code=500)
    
    @staticmethod
    @log
    async def activate(request):
        try:
            token = str(request.query_params['token'])
        except:
            return JSONResponse({"message":"add parameters"}, status_code=500, media_type=media_type)
        if db.user.find({"token": token}).count() == 0: 
            #TODO serve the static
            return JSONResponse({"message":"Wrong token"}, media_type=media_type, status_code=404)
        else:
            db.user.update_one({"token":token}, {'$set': {'is_activated': True}}, upsert=False)
            #TODO serve the static
            return JSONResponse({"message":"success"}, status_code=200, media_type=media_type)
    
    @staticmethod
    @log
    async def password_set(request):
        try:
            data = await request.json()
            token = str(data['token'])
            password = str(data['password'])
        except:
            return JSONResponse({"message":"add parameters"}, status_code=500, media_type=media_type)
        ### Validation Block Open
        if db.user.find({"token": token}).count() == 0: 
            #TODO serve the static
            return JSONResponse({"message":"Wrong token"}, media_type=media_type, status_code=404)
        if len(password) < 6:
            return JSONResponse({"message":"Password must be a string with at least six characters"}, media_type=media_type, status_code=500)
        ### Validation Block Close
        try:
            db.user.update_one({"token":token}, {'$set': {'password_hash': hashlib.sha224(str.encode(password)).hexdigest()}}, upsert=False)
            #TODO serve the static
        except:
            return JSONResponse({"message":"Database error"}, media_type=media_type, status_code=500)
        return JSONResponse({"message":"success"}, status_code=200, media_type=media_type)

class Firebase(HTTPEndpoint):

    @staticmethod
    @log
    async def registration(request):
        try:
            data = await request.json()
            name = str(data['name'])
            email = str(data['email'])
            uuid = str(data['uuid'])
            device_id = str(data['device_id']) if "device_id" in data.keys() else "0"
            platform = str(data['platform']) if "platform" in data.keys() else "0"
            country = str(request.headers['country']) if "country" in request.headers.keys() else "0"
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)

        if db.user.find({"uuid":{"$eq": uuid}}).count() > 0:
            doc = db.user.find_one({"uuid":{"$eq": uuid}})
            data = {"user_id": str(doc["_id"]),
            "device_id": doc["device_id"],
            "uuid": doc["uuid"],
            "token": doc["token"],
            "name": doc["name"],
            "email": doc["email"],
            "gender": "0",
            "platform": doc["platform"],
            "country": doc["country"],
            "notification": doc["notification"],
            "is_host": doc["is_host"],
            "photos": doc["photos"],
            "telegram": doc["telegram"],
            "location": doc["location"],
            "language": doc["language"],
            "activity": doc["activity"],
            "about": doc["about"],
            "caption": doc["caption"],
            "hourly_rate": doc["hourly_rate"],
            "is_deleted": doc["is_deleted"],
            "is_disabled": doc["is_disabled"],
            "reviews": doc["reviews"]}
            return JSONResponse(data, status_code=200, media_type=media_type)
        elif db.user.find({"email":{"$eq": email}}).count() > 0:
            doc = db.user.find_one({"email":{"$eq": email}})
            if uuid != doc['uuid']: # rewrite uuid
                db.user.update_one({'email': email},{'$set': {'uuid': uuid}}, upsert=False)
                doc = db.user.find_one({"email":{"$eq": email}})
            data = {"user_id": str(doc["_id"]),
            "device_id": doc["device_id"],
            "uuid": doc["uuid"],
            "token": doc["token"],
            "name": doc["name"],
            "email": doc["email"],
            "gender": "0",
            "platform": doc["platform"],
            "country": doc["country"],
            "notification": doc["notification"],
            "is_host": doc["is_host"],
            "photos": doc["photos"],
            "telegram": doc["telegram"],
            "location": doc["location"],
            "language": doc["language"],
            "activity": doc["activity"],
            "about": doc["about"],
            "caption": doc["caption"],
            "hourly_rate": doc["hourly_rate"], 
            "is_deleted": doc["is_deleted"],
            "is_disabled": doc["is_disabled"],
            "reviews": doc["reviews"]}
            return JSONResponse(data, status_code=200, media_type=media_type)
        else:
            dt = datetime.utcnow().replace(microsecond=0)
            token = str(uuid4())
            data = {
                    "device_id": device_id,
                    "uuid": uuid,
                    "token": token,
                    "name": name,
                    "email": email,
                    "gender": "0",
                    "platform": platform,
                    "country": country,
                    "notification": {'push': True, 'email': True},
                    "timestamp": {"create_date": dt, "last_login": dt},
                    "is_host": False,
                    "photos": [],
                    "telegram": "",
                    "location": "", # enum
                    "language": [], # enum
                    "activity": [], # enum
                    "about": "",
                    "caption": "",
                    "hourly_rate": 0, 
                    "is_deleted": False,
                    "is_disabled": False,
                    "reviews": []
            }
            _id = db.user.insert(data)
            data['user_id'] = str(_id)
            del data['timestamp']
            return JSONResponse(data, status_code=200, media_type=media_type)
    
    @staticmethod   
    @log
    async def password_reset(request):
        try:
            data = await request.json()
            email = str(data['email'])
        except:
            return JSONResponse({"message":"add parameters"}, status_code=500, media_type=media_type)
        ### Validation Block Open
        if "@" not in email: #TODO make proper validation
            return JSONResponse({"message":"Provided email is invalid"}, media_type=media_type, status_code=500)
        if db.user.find({"email":email}).count() == 0: 
            return JSONResponse({"message":"User does not exists"}, media_type=media_type, status_code=500)
        ### Validation Block Close
        return JSONResponse({"message":"success"}, status_code=200, media_type=media_type)

class User(HTTPEndpoint):
    @staticmethod
    @log
    async def device(request):
        try:
            data = await request.json()
            platform = str(data['platform']) if "platform" in data.keys() else "web"
            version = str(data['version']) if "version" in data.keys() else "web"
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        # if version not in ['2.0.6', 'web']:
        #     links = {
        #         "android": "https://play.google.com/store/apps/details?id=com.vis.client",
        #         "ios": 'https://apps.apple.com/ru/app/%D0%B2%D0%BF%D0%B0%D0%BA%D0%B5%D1%82%D0%B5/id1526944016',
        #         "huawei": "https://appgallery.huawei.com/#/app/C104076545"
        #     }
        #     return JSONResponse({"message": "the app requires an update",
        #         "redirect": "update",
        #         "link": links[platform]
        #         }, status_code=307, media_type=media_type)
        # if platform in ['android']:
        #     return JSONResponse({"message": "shutdown the app",
        #                 "redirect": "shutdown"}, status_code=307, media_type=media_type)
        return JSONResponse({"message": "ok"}, status_code=200)

    @staticmethod
    @log
    async def authentication(request):
        try:
            data = await request.json()
            uuid = str(data['uuid'])
            platform = str(data['platform']) if "platform" in data.keys() else "web"
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)

        # check if user exists
        if db.user.find({"uuid":{"$eq": uuid}}).count() > 0: # TODO add or email
            doc = db.user.find_one({"uuid":{"$eq": uuid}})
            content = {
                # 'token': doc['token'], # TODO добавить шифрование base 64 и чтобы basic auth знал где смотрить логин пароль в crm или в user
                'token': 'Mjk1NzhhOjE2ZjE5ZDM3YWM=',
                'user_id': str(doc['_id']),
                'name': doc['name'],
                'uuid': doc['uuid'],
                'email': doc['email'],
                'notification': {'push': doc['notification']['push'], 'email': doc['notification']['email']},
                "is_host": doc['is_host'],
                "photos": doc['photos'],
                "telegram": doc['telegram'],
                "location": doc['location'], # enum
                "language": doc['language'], # enum
                "activity": doc['activity'], # enum
                "about": doc['about'],
                "caption": doc["caption"],
                "hourly_rate": doc['hourly_rate'], 
                "is_deleted": doc['is_deleted'],
                "is_disabled": doc['is_disabled'],
                "reviews": doc['reviews']
                }
            return JSONResponse(content, status_code=200, media_type=media_type)
        else:
            return JSONResponse({"message": "unauthorized"}, status_code=401, media_type=media_type)

    @staticmethod
    @log
    async def location(request):
        try:
            data = await request.json()
            coordinates = str(data['coordinates']) if "coordinates" in data.keys() else "0 0"
            country = request.headers['country']
        except:
            country = "US"
        return JSONResponse({"country": country}, status_code=200)

    # @requires(['authenticated'], status_code=403)
    @staticmethod
    @log
    async def delete(request):
        try:
            data = await request.json()
            user_id = ObjectId(str(data['user_id']))
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        try:
            db.user.update_one({"_id": user_id}, {'$set': {'is_deleted': True}})
            return JSONResponse({"message": "account deleted"}, status_code=200, media_type=media_type)
        except:
            return JSONResponse({"message": "database error"}, status_code=500, media_type=media_type)

    @staticmethod
    @log
    async def logout(request):
        try:
            data = await request.json()
            user_id = ObjectId(str(data['user_id']))
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)

    @staticmethod
    @log
    async def settings(request):
        '''
        name - string
        location - string 
        rate - string
        caption - string
        about - string 
        caption - string
        language - list
        activity - list
        behost - boolean
        notification - dict
        '''
        try:
            data = await request.json()
            user_id = ObjectId(data['user_id'])
            del data["user_id"]
            possible_params = ['behost','name','location','language','about','notification','activity','rate','caption']
            if len(set(data.keys()) - set(possible_params)) != 0:
                return JSONResponse({"message": "only this params are accepted: 'behost','name','location','language','about','notification','activity','rate','caption'"}, status_code=404)
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        user = db.user.find_one({"_id": user_id})
        if user is None:
            return JSONResponse({"message": "User not found"}, status_code=404)
        validation = {
            "name": str,
            "location": str,
            "rate": str,
            "caption": str,
            "about": str,
            "language": list,
            "activity": list,
            "behost": bool,
            "notification": dict
        }
        for i in data:
            if not isinstance(data[i], validation[i]):
                return JSONResponse({"message": "{} should be {}".format(i,validation[i])}, status_code=500)
        globals().update(data)
        try:
            db.user.update_one({"_id": user_id}, {'$set': data})
        except:
            return JSONResponse({"message": "data not valid"}, status_code=500)
        return JSONResponse({"message": "ok"}, status_code=200)

    @staticmethod
    @log
    async def add_photo(request):
        try:
            data = await request.json()
            user_id = ObjectId(str(data['user_id']))
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)

    @staticmethod
    @log
    async def delete_photo(request):
        try:
            data = await request.json()
            user_id = ObjectId(str(data['user_id']))
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)

class Guide(HTTPEndpoint):
    @staticmethod
    @log
    async def preview(request):
        try:
            data = await request.json()
            _id = data['_id']
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        # TODO валидация if user exist
        doc = db.user.find_one({"_id": ObjectId(_id)})
        content = {
            "_id": str(doc['_id']),
            "name": doc['name'],
            "gender": doc['gender'],
            "photos": doc['photos'],
            "location": doc['location'],
            "language": doc['language'],
            "activity": doc['activity'],
            "about": doc['about'],
            "hourly_rate": doc['hourly_rate'], 
            "amount_reviews": len(doc['reviews']),
            "reviews": doc['reviews']
        }
        return JSONResponse(content, status_code=200)

    @staticmethod
    @log
    async def search(request):
        try:
            data = await request.json()
            _id = data['_id']
            filters = data['filters']
            coordinates = str(data['coordinates']) if "coordinates" in data.keys() else "0"
            city = str(data['city']) if "city" in data.keys() else "0"
            country = data['country']
            page = int(data['page'])
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        # cursor = db.user.find({"location": location}) # TODO filter_by - page, is_host, location
        # content = []
        # for i in cursor:
        #     content.append({
        #         "name": i['name'],
        #         "gender": i['gender'],
        #         "photos": i['photos'],
        #         "location": i['location'],
        #         "about": i['about'],
        #         "hourly_rate": i['hourly_rate'], 
        #         "amount_reviews": len(i['reviews'])
        #     })
        # return JSONResponse(content, status_code=200, media_type=media_type)
        return JSONResponse({"message":"ok"}, status_code=200, media_type=media_type)

    @staticmethod
    @log
    async def add_review(request):
        try:
            data = await request.json()
            _id = ObjectId(data['_id'])
            text = str(data['text'])
        except:
            return JSONResponse({"message": "add parameters"}, status_code=500)
        # TODO validation - list of ObjectIds
        try:
            pass
            # db.user.update_one({"_id": user_id}, {'$set': {"language": language}})
        except:
            return JSONResponse({"message": "data not valid"}, status_code=500)
        return JSONResponse({"message": "ok"}, status_code=200)

class Data(HTTPEndpoint):
    @staticmethod
    @log
    async def activity(request):
        content = {
            "1": "Translation & Interpretation",
            "2": "Pick up & Driving Tours",
            "3": "Shopping",
            "4": "Night life & Bars",
            "5": "Food & Restaurants",
            "6": "Art & Museums",
            "7": "Sports & Recreation",
            "8": "History & Culture",
            "9": "Exploration & Sightseeing"
        }
        return JSONResponse(content, status_code=200)

    @staticmethod
    @log
    async def language(request):
        content = {
            "1": "English",
            "2": "Spanish",
            "3": "Italian",
            "4": "Russian",
            "5": "Chineese"
        }
        return JSONResponse(content, status_code=200)

routes = [
Route("/mail/login", endpoint=Mail.login, methods=methods),
Route("/mail/registration", endpoint=Mail.registration, methods=methods),   
Route("/mail/activate", endpoint=Mail.activate, methods=["GET"]),    
Route("/mail/password/reset", endpoint=Mail.password_reset, methods=methods),
Route("/mail/password/set", endpoint=Mail.password_set, methods=methods),

Route("/firebase/registration", endpoint=Firebase.registration, methods=methods),
Route("/firebase/password/reset", endpoint=Firebase.password_reset, methods=methods), #TODO

Route("/user/device", endpoint=User.device, methods=methods),
Route("/user/authentication", endpoint=User.authentication, methods=methods),
Route("/user/location", endpoint=User.location, methods=methods),
Route("/user/delete", endpoint=User.delete, methods=methods),
Route("/user/logout", endpoint=User.logout, methods=methods),
Route("/user/settings", endpoint=User.settings, methods=methods),
Route("/user/add/photo", endpoint=User.add_photo, methods=methods), 
Route("/user/delete/photo", endpoint=User.delete_photo, methods=methods),  

Route("/guide/preview", endpoint=Guide.preview, methods=methods),
Route("/guide/search", endpoint=Guide.search, methods=methods),
Route("/guide/add/review", endpoint=Guide.add_review, methods=methods),

Route("/activity/list", endpoint=Data.activity, methods=methods),
Route("/language/list", endpoint=Data.language, methods=methods),

# Route("/firebase/send", endpoint=Push.send, methods=methods),
]

middleware = [
    Middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'], allow_headers=['*']),
    Middleware(AuthenticationMiddleware, backend=BasicAuthBackend()),
    Middleware(CustomHeaderMiddleware),
]

app = Starlette(routes=routes, middleware=middleware)

app.add_middleware(
    TimingMiddleware,
    client=PrintTimings(),
    metric_namer=StarletteScopeToName(prefix="core", starlette_app=app)
)